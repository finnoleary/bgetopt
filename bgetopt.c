#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>

/* first, things come in three parts:
 *
 *   [pargs] <nargs> [args] <optargs>
 *
 * we need length(optargs) to determine the size of the optarray
 * Store pargs for parsing options
 * Then we loop through and cut up args into the optarray
 *
 */

#define throw(s) { fprintf(stderr, s); goto _throw; }
#define mthrow(s,...) { fprintf(stderr, s, ##__VA_ARGS__); goto _throw; }

#define MAXARG 256

size_t nargs, noptargs;
const char **args, **optargs;

struct {
	/* --no-combine, -n: Treat -cvf as --cvf */
	int no_combine;
} Options = {
	0
};

typedef struct {
	int isvar; int used;
	char sinopt; char mulopt[MAXARG]; char var[MAXARG]; char *action;
} StrOption; /* This is a bad name :P */

static void help(char *);
static int findloc(int, char **);
static int fillopts(StrOption *);

int main(int argc, char **argv)
{
	StrOption *opts = NULL;
	if (argc == 1
	|| (strcmp(argv[1], "--help")) == 0
	|| (strcmp(argv[1], "-h")) == 0) {
		help(argv[0]); exit(0);
	}
	if (!findloc(argc, argv)) goto _throw;

	if (!nargs || !noptargs) throw("No args!");
	
	opts = calloc(noptargs, sizeof(StrOption));
	if (!opts) goto _throw;

	if (!fillopts(opts)) goto _throw;

	size_t i = 0;
	for (i = 0; i < nargs; i++)
		printf("single: '%c', multi: '%s', variable: '%s'\n",
		       opts[i].sinopt, opts[i].mulopt, opts[i].var);
	
	free(opts);
	return 0;
_throw:
	perror(argv[0]);
	if (opts) free(opts);
	return 1;
}

static int yank_arg(char *d, const char *s, size_t len)
{
	size_t i;
	for (i = 0; (isalpha(s[i]) || s[i] == '_') && i < len; i++)
		d[i] = s[i];

	if (!isalpha(s[i] && s[i] != '_'))
		goto _invalid;

	return i;

_invalid:
	fprintf(stderr, "Invalid variable option syntax: '%s'\n", s);
	return -1;
}

static int parse_single(StrOption *opt, const char *s, size_t len)
{
	if (1 > len) goto _invalid;
	if (s[0] != '-') goto _invalid;
	if (s[1] != '-') return 0;
	if (!isalpha(s[1])) goto _invalid;
	opt->sinopt = s[1];
	if (s[2] == '|') return 2;
	if (s[2] != '=') goto _invalid;
	int n = yank_arg(opt->var, s+2+1, len-2);

	return n+2;

_invalid:
	fprintf(stderr, "Invalid single option syntax: '%s'\n", s);
	return -1;
}

static int parse_multi(StrOption *opts, const char *s, size_t len)
{
	if (3 > len) goto _invalid;
	if (s[0] != '-' && s[1] != '-') goto _invalid;
	if (!isalpha(s[1])) goto _invalid;
	int n0 = yank_arg(opts->mulopt, s+3, len-3);
	n0 += 3;
	if (s[n0] != '=') return n0;
	int n1 = yank_arg(opts->mulopt, s+n0+1, len-n0);
	
	return n0+n1;

_invalid:
	fprintf(stderr, "Invalid multiple option syntax: '%s'\n", s);
	return -1;
}

int nextbrk(const char *s, size_t i, size_t len)
{
	for (; i < len; i++)
		if (s[i] == '|' || s[i] == ';') break;

	if (i == len) { return -1; }
	return i;
}

static int fillopts(StrOption *opts)
{
	int i, j, n; size_t len;
	for (i = 0; i < noptargs; i++) {
		if ((len = strlen(optargs[i])) == 0) continue;

		if ((j = nextbrk(optargs[i], 0, len)) < 0)
			mthrow("Bad option: '%s'\n", optargs[i]);

		if ((n = parse_single((opts+i), optargs[i], j)) < 0) {
			goto _throw;
		}
		if (n != 0) {
			i = j+1; 
			if ((j = nextbrk(optargs[i], i, len)) < 0)
				mthrow("Bad option: '%s'\n", optargs[i]);
		}
		if ((n = parse_multi((opts+i), optargs[i], j)) < 0) {
			goto _throw;
		}
	}
	return 1;
_throw:
	return 0;
}

static int findloc(int pargc, char **pargs)
{

	/*** Find the locations of the things we intend to parse ***/

	int i;
	/* find location of nargs */
	for (i = 1; i < pargc; i++) {
		if (isdigit(*pargs[i])) {
			args = pargs+i+1;
			errno = 0;
			nargs = strtol(pargs[i], (void*)NULL, 10);
			if (!nargs && errno) { throw("nargs invalid\n"); }
		}
	}
	if (!args) { throw("no nargs found!\n"); }
	/* find the number of optargs */
	optargs = args+nargs;
	noptargs = (i+nargs) - pargc;

	/* printf(stderr, "args: %ld (%s -- %s); optargs: %ld (%s -- %s);\n",
	 *        nargs, args[0], args[2], noptargs, optargs[0], optargs[2]);
	 */
	return 1;
_throw:
	return 0;
}

static void help(char *pname)
{
	const char *helpstring =
"<nargs> <args ...> <options ...>\n\
Example: bgetopt \"$#\" \"$@\" \"-v|--version; echo '1.0.0'\" \\\n \
                 \"-s|--string=S; echo \\\"S\\\"\"";
	
	printf("%s %s\n", pname, helpstring);
}

