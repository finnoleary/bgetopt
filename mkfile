CC=gcc
CFLAGS= -D_XOPEN_SOURCE=700 -D_FORTIFY_SOURCE=2 -std=c99 -Wall -Wextra -pedantic -Os -fstack-protector-strong -lrt -g
CPPCHKFLAGS= --std=posix --std=c99 --platform=unix64 --enable=all
SPARSEFLAGS= -Wsparse-all
SCANBUILDFLAGS= -enable-checker core -enable-checker security -enable-checker unix
SCANBUILD=scan-build 

do:V:
  $SCANBUILD $CC $CFLAGS *.c -o bgetopt

check:V:
  sparse $SPARSEFLAGS bgetopt.c
  cppcheck $CPPCHKFLAGS bgetopt.c

test:V:
  $SCANBUILD $CC $CFLAGS test_*.c -o test_bgetopt

clean:V:
  rm bgetopt
