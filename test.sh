#!/bin/mksh

options=("-c=x|--color=x; echo \"color is now $x\""
         "-v|--verbose; verbose=1")

./bgetopt "$#" "$@" "${options[@]}"

# ./bgetopt "$#" "$@" "-c|--color=x; echo 'hello world'" \
#                     "-v|--verbose; verbose=1"

# echo $verbose
